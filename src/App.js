import React from 'react';
import './App.css';
import Header from "./components/Header/Header";
import ChatContainer from "./components/Chat/ChatContainer";
import Footer from "./components/Footer/Footer";

function App() {
  return (
    <div className="App">
      <Header />
      <ChatContainer />
      <Footer />
    </div>
  );
}

export default App;
