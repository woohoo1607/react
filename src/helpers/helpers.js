export const unique = (arr) => {
  return Array.from(new Set(arr));
}

export const getDate = () => {
  let newDate = new Date();
  return newDate.toISOString()
}
