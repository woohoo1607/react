import React, {useState} from 'react';

const ChatFooter = ({sendMsg}) => {
  let [msg, setMsg] = useState('');
  const send = () => {
    sendMsg(msg);
    setMsg('');
  };
  const changeInput = (e)  => {
    setMsg(e.target.value);
  }
  return (
      <div className='chatFooter'>
        <input onChange={changeInput} value={msg} />
        <button onClick={send}>Send</button>
      </div>
  )
};

export default ChatFooter;
