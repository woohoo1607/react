import React, {useEffect, useRef, useState} from 'react';

import Message from "../Message/Message";
import TimeLine from "../TimeLine/TimeLine";

const ChatBody = ({messages, myId, editMsg, deleteMsg, liked, likes, ...props}) => {
  const messagesEndRef = useRef(null);
  let timelineDate = 0;
  const scrollToBottom = () => {
    messagesEndRef.current.scrollIntoView({ behavior: "smooth" })
  }

  useEffect(scrollToBottom, [messages]);
  return (
      <div className='chatBody'>
        {messages.map(message => {
          const messageLikes = likes.filter(like=>like.msgId===message.id);
          const date = new Date(message.createdAt)
          const msgDay = date.getDay();
          if (msgDay!==timelineDate) {
            timelineDate=msgDay;
            return (
                <>
                  <TimeLine date={date}/>
                  <Message key={message.id}
                           message={message}
                           myId={myId}
                           editMsg={editMsg}
                           deleteMsg={deleteMsg}
                           liked={liked}
                           messageLikes={messageLikes}
                  />
                </>
            )
          }

          console.log((new Date(message.createdAt)))
          return <Message key={message.id}
                   message={message}
                   myId={myId}
                   editMsg={editMsg}
                   deleteMsg={deleteMsg}
                   liked={liked}
                   messageLikes={messageLikes}
          />
        })}
        <div ref={messagesEndRef}></div>
      </div>
  )
};
export default ChatBody;
