import React, {useEffect, useState} from 'react';
import { uuid } from 'uuidv4';

import './styles.css';
import ChatFooter from "./ChatFooter";
import ChatHeader from "./ChatHeader";
import {chatAPI} from "../../api/api";
import {getDate, unique} from "../../helpers/helpers";
import ChatBody from "./ChatBody";
import Preloader from "../Preloader/Preloader";

const ChatContainer = () => {

  let [isFetching, setIsFetching] = useState(false);
  let [myId, setMyId] = useState(null);
  let [messages, setMessages] = useState([]);
  let [likes, setLikes] = useState([]);

  useEffect(() => {
    setIsFetching(true);
    (async () => {
      let res = await chatAPI.getChatData();
      setMessages(res)
      setMyId(res[res.length-1].userId);
      setIsFetching(false);
    })();
  }, []);
console.log(messages);

  const quantityUsers = unique(messages.map(user => user.userId)).length;
  const lastTimeMsg = messages.length ? messages[messages.length - 1].createdAt : false;
  const myData = messages.find(message => message.userId ===myId);

  const editMsg = (msgId, text) => {
    let copyMessages = [...messages];
    const msgIndex = copyMessages.findIndex(message=>message.id===msgId);
    copyMessages[msgIndex].text = text;
    copyMessages[msgIndex].editedAt = getDate();
    setMessages(copyMessages);
  }

  const deleteMsg = (msgId) => {
    let copyMessages = [...messages];
    const msgIndex = copyMessages.findIndex(message=>message.id===msgId);
    copyMessages.splice(msgIndex, 1);
    setMessages(copyMessages);
  }

  const sendMsg = (msg) => {
    const newMessage = {
      id: uuid(),
      text: msg,
      avatar: myData.avatar,
      user: myData.user,
      userId: myId,
      editedAt: "",
      createdAt: getDate()
    }
    let newMessages = [...messages]
    newMessages.push(newMessage);
    setMessages(newMessages);
  }

  const liked = (msgId, userId) => {
    let copyLikes = [...likes];
    const likeIndex = copyLikes.findIndex(like=> like.msgId===msgId && like.userId===userId);
    if (likeIndex===-1) {
      const newLike = {
        msgId,
        userId,
        isLike: true,
      };
      copyLikes.push(newLike);
      setLikes(copyLikes);
    } else {
      copyLikes.splice(likeIndex, 1);
      setLikes(copyLikes);
    }
  }


  return (
      <div className='center'>
        <div className='chatContainer'>
          {isFetching && <Preloader />}
          {!isFetching && <>
            <ChatHeader title='My chat'
                        quantityUsers={quantityUsers}
                        quantityMessages={messages.length}
                        lastTimeMsg={lastTimeMsg}
            />
            <ChatBody messages={messages}
                      myId={myId}
                      editMsg={editMsg}
                      deleteMsg={deleteMsg}
                      liked={liked}
                      likes={likes}
            />
            <ChatFooter sendMsg={sendMsg}/>
          </>
          }
        </div>
      </div>
  )
};

export default ChatContainer;
