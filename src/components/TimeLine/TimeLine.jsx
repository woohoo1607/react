import React from 'react';
import Moment from 'react-moment';
import 'moment-timezone';

import './styles.css';

const TimeLine = ({date}) => {
  return (
      <div className='timeline'>
        <span><Moment format='YYY-MM-DD' date={date}/></span>
      </div>
  )
}
export default TimeLine;
