import React, {useState} from 'react';
import Moment from 'react-moment';
import 'moment-timezone';

import Like from '../../image/like.png'
import './styles.css';

const Message = ({message, myId, editMsg, deleteMsg, liked, messageLikes, ...props}) => {
  let [isEditMode, setIsEditMode] = useState(false);
  let [msg, setMsg] = useState(message.text);
  let isLiked = false;
  if (messageLikes.find(like=> like.userId===myId)) {
    /*setIsILiked(true);*/
    isLiked=true
  }
  const changeEditMode = () => {
    setIsEditMode(!isEditMode);
  };

  const cancelEdit = () => {
    setMsg(message.text);
    setIsEditMode(false)
  }

  const saveMsg = () => {
    editMsg(message.id, msg)
    changeEditMode();
  }

  const deleteMessage = () => {
    deleteMsg(message.id);
  }

  const changeInput = (e)  => {
    setMsg(e.target.value);
  }

  const likeMsg = () => {
    liked(message.id, myId)
  }

  let isMyMsg = message.userId===myId ? true : false;
  return (
      <div className={isMyMsg ? 'message right' : 'message'}>
        {!isMyMsg && <img src={message.avatar} alt='avatar'/>}
        <div className='bodyMessage'>
          <div className='bodyMessageTitle'>
            <h3>{message.user}</h3>
            <p><Moment format='hh:mm:ss' date={message.createdAt}/></p>
            {isMyMsg && <>
              {!isEditMode && <button onClick={changeEditMode}>edit</button>}
              {isEditMode && <button onClick={saveMsg}>save</button>}
              {isEditMode && <button onClick={cancelEdit}>cancel</button>}
                <button onClick={deleteMessage}>delete</button>
              </>
            }
          </div>
          {!isEditMode && <p>{message.text}</p>}
          {isEditMode && <textarea onChange={changeInput} value={msg}/>}
          <div className='messageInfo'>
            {!isMyMsg && <img onClick={likeMsg} src={Like} height='10px' alt='like'/>}
            {isLiked && <span>you like</span>}
          </div>
        </div>
      </div>
  )
};
export default Message;
